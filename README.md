# Hands-on Meetup Docker

## Instalação

MacOS:

https://docs.docker.com/docker-for-mac/install/

Windows:

https://docs.docker.com/docker-for-windows/install/

Linux:

https://docs.docker.com/engine/install/

Para sistemas baseados em Linux temos um script que facilita a instalação:

```console
curl -fsSL https://get.docker.com | sh
```

## Play with Docker

É possível "brincar" com Docker sem instalar nada no seu computador ou gastar com Cloud. Basta criar gratuitamente uma conta no [Docker Hub](https://hub.docker.com/signup/).

https://labs.play-with-docker.com/

## Comandos Básicos Docker

http://dockerlabs.collabnix.com/docker/cheatsheet/

https://www.docker.com/sites/default/files/d8/2019-09/docker-cheat-sheet.pdf

https://dockerlux.github.io/pdf/cheat-sheet-v2.pdf

### Comandos de Gerenciamento

Exibir informações de sistema:

```console
docker info
```

Retorna informações de baixo nível sobre objetos do Docker:

```console
docker inspect [OPTIONS] NAME|ID [NAME|ID...]
```

Mostra informações da versão do docker:

```console
docker version
```

### Comandos de imagens Docker

Pesquisar imagens no Docker Hub:

```console
docker search TERM
```

Obter imagem de registry:

```console
docker image pull NAME:TAG
```

Listar imagens:

```console
docker image ls
```

Remover imagem:

```console
docker image rm IMAGE
```

### Comandos de containers Docker

Criar um container:

```console
docker container create IMAGE [COMMAND] [ARG...]
```

Iniciar um ou mais containers parados:

```console
docker container start [OPTIONS] CONTAINER [CONTAINER...]
```

Listar containers:

```console
docker container ls
```

ou

```console
docker ps
```

Rodar um comando em um container em execução:

```console
docker container exec CONTAINER COMMAND [ARG...]
```

Matar container(s) em execução:

```console
docker container kill CONTAINER [CONTAINER...]
```

Trás os logs de um container:

```console
docker container logs CONTAINER
```

Pausa todos os processos dentro de container(s):

```console
docker container pause CONTAINER [CONTAINER...]
```

Des-pausar todos os processos dentro de um container:

```console
docker container unpause CONTAINER [CONTAINER...]
```

Lista mapeamento de portas de um container:

```console
docker container port CONTAINER
```

Renomear um container:

```console
docker container rename CONTAINER NEW_NAME
```

Attach para um container rodando:

```console
docker container attach CONTAINER
```

Copia arquivos/diretórios de um container para o host:

```console
docker cp
```

Restartar container em execução:

```console
docker container restart CONTAINER [CONTAINER...]
```

Parar container(s) em execução:

```console
docker container stop CONTAINER [CONTAINER...]
```

Remove um ou mais containers:

```console
docker container rm CONTAINER [CONTAINER...]
```

Rodar um comando em um novo container:	

```console
docker container run [OPTIONS] IMAGE [COMMAND] [ARG...]
```

Exibir um stream das estatísticas de uso de container(s):

```console
docker stats
```

Exibir processos de um container:

```console
docker container top CONTAINER
```

### Comandos de volumes Docker

Listar Volumes:

```console
docker volume ls
```

Criar um volume:

```console
docker volume create [OPTIONS] [VOLUME]
```

Remover um ou mais volumes:

```console
docker volume create [OPTIONS] [VOLUME]
```

Inspecionar um ou mais volumes:

```console
docker volume inspect [OPTIONS] VOLUME [VOLUME...]
```

### Comandos de network Docker

Listar redes:

```console
docker network ls [OPTIONS]
```

Criar uma rede:

```console
docker network create [OPTIONS] NETWORK
```

Conectar um container em uma rede:

```console
docker network connect [OPTIONS] NETWORK CONTAINER
```

Desconectar um container de uma rede:

```console
docker network disconnect [OPTIONS] NETWORK CONTAINER
```

Remover uma ou mais redes:

```console
docker network rm NETWORK [NETWORK...]
```

## Dockerfile

https://docs.docker.com/engine/reference/builder/

Exemplo de Dockerfile:

```txt
# our base image
FROM alpine:3.5

# Install python and pip
RUN apk add --update py2-pip

# install Python modules needed by the Python app
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

# copy files required for the app to run
COPY app.py /usr/src/app/
COPY templates/index.html /usr/src/app/templates/

# tell the port number the container should expose
EXPOSE 5000

# run the application
CMD ["python", "/usr/src/app/app.py"]
```

## Docker Compose

https://docs.docker.com/compose/

Exemplo de docker-compose.yml:

```txt
version: '3'
services:
  php-apache:
    image: php:7.2.1-apache
    ports:
      - 80:80
    volumes:
      - ./DocumentRoot:/var/www/html:z
    links:
      - 'mariadb'

  mariadb:
    image: mariadb:10.1
      volumes:
        - mariadb:/var/lib/mysql
      environment:
        TZ: "Europe/Rome"
        MYSQL_ALLOW_EMPTY_PASSWORD: "no"
        MYSQL_ROOT_PASSWORD: "rootpwd"
        MYSQL_USER: 'testuser'
        MYSQL_PASSWORD: 'testpassword'
        MYSQL_DATABASE: 'testdb'

volumes:
  mariadb:
```

### Comandos básicos do docker-compose

Listar containers:

```console
docker-compose ps [options] [SERVICE...]
```

Constrói, (re)cria, inicia e vincula containers para um serviço:

```console
docker-compose up [options] [--scale SERVICE=NUM...] [SERVICE...]
```

Para e remove containers, redes, volumes e imagens criadas pelo "up":

```console
docker-compose down [options]
```

Executa um comando em um container rodando:

```console
docker-compose exec [options] [-e KEY=VAL...] SERVICE COMMAND [ARGS...]
```

## Labs e Tutoriais

### Docker 101

https://www.docker.com/101-tutorial

## License

GPLv3 or later.
